#!/bin/sh


if ! test -d website/output
then
    echo "Website-Build not found"
    exit 1
fi


echo "SSH_USER=${SSH_USER}"
echo "SSH_HOST=${SSH_HOST}"
if [ -n "$LFTP_PASSWORD" ]
then
    echo "Password for Deployment found"
fi


lftp -e 'set sftp:auto-confirm yes; mirror -R ./website/output/ ./web/; bye' --env-password -u ${SSH_USER} sftp://${SSH_HOST}


