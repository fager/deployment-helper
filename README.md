# deployment-helper für Nikola based Websites

# ENV-Variables

* LFTP_PASSWORD (Deployment-Passwort for sftp-Upload)
* SSH_HOST (Deployment-Host for sftp-Upload)
* SSH_USER (Deployment-User for sftp-Upload)

## Python 3.11 and Nikola 8.x

This image is based on the python:3.11-alpine image and includes additional
Python-packages for building nikola-sites.

## lftp for sftp-Uploads

This image includes `lftp` to upload files to a sftp-server and use
the sftp-password from LFTP_PASSWORD environment variable.

```bash
lftp -e 'set sftp:auto-confirm yes; mirror -R ./<LOCAL_DIR>/ ./<REMOTE_DIR>/; bye' --env-password -u USER sftp://<TARGET_FQDN>
```

