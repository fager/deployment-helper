#!/bin/sh

if ! test -d website
then
    echo "Website directory not found"
    exit 1
fi

if ! command -v nikola >/dev/null 2>&1
then
    echo "nikola command not found"
    exit 1
fi

cd website

nikola build
exit "$?"
