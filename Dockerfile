FROM docker.io/library/python:3.11-alpine

RUN apk --no-cache add sshpass openssh-client lftp; mkdir -p /opt/app
WORKDIR /opt/app
ADD requirements.txt /root/
ADD Dockerfile /root/
ADD bin/*.sh /usr/local/bin/

# ignore "Running pip as root" warning
ENV PIP_ROOT_USER_ACTION=ignore

RUN chmod 755 /usr/local/bin/*.sh && pip install -r /root/requirements.txt && pip freeze && rm -Rf "$(pip cache dir)"

